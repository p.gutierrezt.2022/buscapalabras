#!/usr/bin/env pythoh3

'''
Busca una palabra en una lista de palabras
'''

import sys
import sortwords

def search_word(word, words_list):
    encontrado = False
    for i in words_list:

        if sortwords.equal(word, i):
            index = words_list.index(i)
            encontrado = True

    if not encontrado:
        raise Exception

    return index

def main():
    word = sys.argv[1]
    words_list = sys.argv[2:]

    if len(sys.argv) < 3:
        sys.exit('se necesita de al menos dos argumentos')

    try:
        ordered_list = sortwords.sort(words_list)
        position = search_word(word, words_list)
        sortwords.show(ordered_list)
        print(position)

    except Exception:
        sys.exit('palabra no encontrada')

if __name__ == '__main__':
    main()

